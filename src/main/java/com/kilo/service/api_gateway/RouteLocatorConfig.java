//package com.kilo.service.api_gateway;
//
//import org.springframework.cloud.gateway.route.RouteLocator;
//import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @author Sombath
// * create at 12/7/24 4:13 PM
// */
//
//@Configuration
//public class RouteLocatorConfig {
//
//    @Bean
//    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
//        return builder.routes()
//                .route("chat-service", r -> r.path("/chat/**")
//                        .filters(f -> f.stripPrefix(1))
//                        .uri("lb://CHAT-SERVICE"))
//                .build();
//    }
//
//}
