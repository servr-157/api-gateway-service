pipeline {
    agent any

    tools {
        maven 'MAVEN 3.9.8'
    }

    environment {
        COMPOSE_PATH = '../../../../root/docker-compose/server-157'
        SERVICE_NAME = 'gateway-service'
        GROUP_NAME = 'server-157'
    }

    options {
        gitlabBuilds(builds: ['build', 'deploy'])
    }

    stages {
        stage('build') {
            steps {
                echo "\n"
                echo "git branch: ${env.gitlabSourceBranch}, Action: ${env.gitlabActionType}"
                echo "\n"

                gitlabCommitStatus(name: 'build') {
                    sh 'mvn -B -DskipTests clean package'
                    sh "docker buildx build --platform linux/amd64,linux/arm64 --tag ${GROUP_NAME}/${SERVICE_NAME} ."
                }
            }
        }

        stage('deploy') {
            steps {
                gitlabCommitStatus(name: 'deploy') {
                    sh "cd ${COMPOSE_PATH} && docker compose up -d --no-deps ${SERVICE_NAME}"
                }
            }
        }

    }

    post {
        always {
            cleanWs()
        }
    }
}
